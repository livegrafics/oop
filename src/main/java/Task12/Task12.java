package Task12;

import java.math.BigInteger;

public class Task12 {
    public static void main(String[] args) {
        System.out.println("Max LONG: "+Long.MAX_VALUE);
        long startValue=Math.round(Math.sqrt(Long.MAX_VALUE));
        BigInteger bigInteger;
        for (int i=1;i<=10;i++){
            bigInteger=new BigInteger(Long.toString(startValue+i));
            bigInteger=bigInteger.multiply(bigInteger);
            System.out.println(bigInteger);
        }
    }
}
