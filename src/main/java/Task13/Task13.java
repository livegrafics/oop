package Task13;

import java.math.BigInteger;

public class Task13 {
    public static void main(String[] args) {
        BigInteger bigInteger=new BigInteger(Long.toString(Long.MAX_VALUE)).add(new BigInteger("1"));
        System.out.println(Long.MAX_VALUE);
        System.out.println(bigInteger);
        int count=1;
        boolean flag=false;
        while (count<=5){
            BigInteger testNum=new BigInteger("2");
            flag=false;
            while(testNum.compareTo(bigInteger)==-1 && !flag){
                if (bigInteger.mod(testNum).compareTo(new BigInteger("0"))==0){
                    flag=true;
                }
                testNum=testNum.add(new BigInteger("1"));
                ;
            }
            if (!flag && bigInteger.compareTo(testNum)==0){
                System.out.println(bigInteger+" - �������");
                count++;
            }
            bigInteger=bigInteger.add(new BigInteger("1"));
        }
    }
}
