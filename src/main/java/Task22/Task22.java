package Task22;

import java.util.Scanner;

public class Task22 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        System.out.println("Введите значения сторон: ");
        double side1=scanner.nextDouble();
        double side2= scanner.nextDouble();
        double side3= scanner.nextDouble();
        System.out.println("Введите статус закрашивания (true/false): ");
        boolean isFilled = scanner.nextBoolean();
        System.out.println("Укажите цвет заливки: ");
        String color=scanner.next();

        Triangle triangle=new Triangle(side1,side2,side3);
        triangle.setColor(color);
        triangle.setFilled(isFilled);
        System.out.println("Площадь треугольника: "+triangle.getArea());
        System.out.println("Периметр треугольника: "+triangle.getPerimeter());
        System.out.println("Цвет: "+triangle.getColor());
        System.out.println("Статус закрашивания: "+triangle.isFilled());
    }
}
