package Task3;

import java.util.Date;

public class Task3 {
    public static void main(String[] args) {
        System.out.println(new Date(10000L).toString());
        System.out.println(new Date(100000L).toString());
        System.out.println(new Date(1000000L).toString());
        System.out.println(new Date(10000000L).toString());
        System.out.println(new Date(100000000L).toString());
        System.out.println(new Date(1000000000L).toString());
        System.out.println(new Date(10000000000L).toString());
        System.out.println(new Date(100000000000L).toString());

    }
}
