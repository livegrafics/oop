package Task10;

import java.util.GregorianCalendar;

public class MyDate {
    private int year, month, day;
    private long totalMilliSecond;
    GregorianCalendar gregorianCalendar;

    public MyDate() {
        gregorianCalendar = new GregorianCalendar();
        getDate(gregorianCalendar);
    }

    public MyDate(long totalMilliSecond) {
        gregorianCalendar = new GregorianCalendar();
        setDate(totalMilliSecond);
        getDate(gregorianCalendar);
    }

    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public void getDate(GregorianCalendar gregorianCalendar){
        year = gregorianCalendar.get(GregorianCalendar.YEAR);
        month = gregorianCalendar.get(GregorianCalendar.MONTH)+1;
        day = gregorianCalendar.get(GregorianCalendar.DAY_OF_MONTH);
    }

    public void setDate(long elapsedTime){
        gregorianCalendar.setTimeInMillis(totalMilliSecond);
        getDate(gregorianCalendar);
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

}
