package Task11;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

public class Task11 {
    public static void main(String[] args) {
        BigDecimal bigDecimal=BigDecimal.ONE;
        BigInteger bigInteger;
        String initialString;
        for (int i=1;i<50;i++){
            bigDecimal=bigDecimal.multiply(new BigDecimal("10"));
            System.out.println(bigDecimal);
        }
        int count=0;
        do{
            bigInteger=bigDecimal.toBigInteger();
            if (bigInteger.mod(new BigInteger("2")).compareTo(new BigInteger("0"))==0 || bigInteger.mod(new BigInteger("3")).compareTo(new BigInteger("0"))==0){
                count++;
                System.out.println(bigInteger+" "+count);
            }
            bigDecimal=bigDecimal.add(new BigDecimal("1"));
        }while(count<10);
    }

}
