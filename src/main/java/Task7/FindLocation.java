package Task7;

import java.util.Scanner;

public class FindLocation {
    public static void main(String[] args) {
        double[][] array;
        Scanner scanner = new Scanner(System.in);
        System.out.println("������� ���������� ������� � �������� �������: ");
        int size_row, size_column;
        size_row = scanner.nextInt();
        size_column = scanner.nextInt();
        array = new double[size_row][size_column];
        System.out.println("������� ������: ");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = scanner.nextDouble();
            }
        }
        Location location;
        location = locateLargest(array);
        System.out.println("���������� ������� �������, ������ " + location.maxValue
                + " , ��������� � ������� (" + location.row + "," + location.column + ")");
    }

    public static Location locateLargest(double[][] a) {
        Location location = new Location();
        location.maxValue = a[0][0];
        location.row = 0;
        location.column = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                if (location.maxValue < a[i][j]) {
                    location.maxValue = a[i][j];
                    location.row = i;
                    location.column = j;
                }
            }
        }
        return location;
    }
}
