package Task26;

import java.util.ArrayList;
import java.util.Random;

public class Task26 {
    public static void main(String[] args) {
        int[][] array;
        Random random = new Random(System.currentTimeMillis());
        int n = random.nextInt(30) + 1;
        array = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = random.nextInt(2);
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        ArrayList<Integer> rows = new ArrayList<>();
        ArrayList<Integer> columns = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            int summ = 0;
            for (int j = 0; j < array[0].length; j++) {
                summ = summ + array[i][j];
            }
            rows.add(summ);
        }
        for (int j = 0; j < array[0].length; j++) {
            int summ = 0;
            for (int i = 0; i < array.length; i++) {
                summ = summ + array[i][j];
            }
            columns.add(summ);
        }
        System.out.println("Номер строки с наибольшим количеством единиц: " + maxIndex(rows));
        System.out.println("Номер столбца с наибольшим количеством единиц: " + maxIndex((columns)));
    }

    public static int maxIndex(ArrayList<Integer> list) {
        int maxIndex = 0;
        int max = list.get(maxIndex);
        for (int i = 1; i < list.size(); i++) {
            if (max < list.get(i)) {
                max = list.get(i);
                maxIndex = i;
            }
        }
        return maxIndex;
    }
}
