package Task14;

import java.math.BigInteger;

public class Task14 {
    public static void main(String[] args) {
        System.out.println("\tp\t\t2^p-1");
        System.out.println("------------------------");
        for (int p=1;p<100;p++){
            boolean isSimple=true;
            for (int i=2;i<p;i++){
                if (p%i==0){
                    isSimple=false;
                    break;
                }
            }
            if (isSimple){
                BigInteger bigInteger=BigInteger.ONE;
                for (int i=1;i<=p;i++){
                    bigInteger=bigInteger.multiply(new BigInteger("2"));
                }
                bigInteger=bigInteger.subtract(new BigInteger("1"));
                System.out.println("\t"+p+"\t\t"+bigInteger);
            }
        }
    }
}
