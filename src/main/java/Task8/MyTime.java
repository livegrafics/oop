package Task8;

import java.util.Calendar;
import java.util.Date;

public class MyTime {
    private int hour;
    private int minute;
    private int second;

    final int MILLISECONDS_PER_SECOND = 1000, SECONDS_PER_MINUTE = 60, MINUTES_PER_HOUR = 60, HOURS_PER_DAY = 24;

    private long totalTimeInMilliSecond;
    private Date date;

    public MyTime() {
        totalTimeInMilliSecond = System.currentTimeMillis();
        convertMiliSecondToRealTime();
    }

    public MyTime(long elapsedTimeInMilliSecond) {
        this.totalTimeInMilliSecond = System.currentTimeMillis();
        setTime(elapsedTimeInMilliSecond);
        convertMiliSecondToRealTime();
    }

    public MyTime(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public void convertMiliSecondToRealTime() {
        long totalSecond = totalTimeInMilliSecond / MILLISECONDS_PER_SECOND;
        second = (int) (totalSecond % SECONDS_PER_MINUTE);
        long totalMinutes = totalSecond / SECONDS_PER_MINUTE;
        minute = (int) (totalMinutes % MINUTES_PER_HOUR);
        long totalHours = totalMinutes / MINUTES_PER_HOUR;
        hour = (int) (totalHours % HOURS_PER_DAY);
    }

    public void setTime(long elapsedTimeInMilliSecond) {
        totalTimeInMilliSecond = totalTimeInMilliSecond + elapsedTimeInMilliSecond;
        convertMiliSecondToRealTime();
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }
}
