package Task25;

import java.util.ArrayList;
import java.util.Random;

public class Task25 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
            System.out.print(i + " ");
        }
        System.out.println();
        shuffle(list);
        for (Integer i : list) {
            System.out.print(i + " ");
        }
    }

    public static void shuffle(ArrayList<Integer> list) {
        Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < list.size(); i++) {
            int j = (int) random.nextInt(10);
            Integer temp = list.get(i);
            list.set(i, list.get(j));
            list.set(j, temp);
        }
    }
}
