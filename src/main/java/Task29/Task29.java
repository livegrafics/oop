package Task29;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Task29 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> list = new ArrayList<>();
        System.out.print("Введите десять чисел: ");
        for (int i = 0; i < 10; i++) {
            list.add(scanner.nextInt());
        }
        removeDuplicate(list);
    }

    public static void removeDuplicate(ArrayList<Integer> list) {
        ArrayList<Integer> tempList = new ArrayList<>();
        tempList.add(list.get(0));
        for (int i = 1; i < list.size(); i++) {
            if (!tempList.contains(list.get(i))) {
                tempList.add(list.get(i));
            }
        }
        list = tempList;
        System.out.println(Arrays.toString(list.toArray()));
    }
}
