package Task30;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Task30 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> list1 = new ArrayList<>();
        ArrayList<Integer> list2 = new ArrayList<>();
        System.out.print("Введите пять чисел для списка1: ");
        for (int i = 0; i < 5; i++) {
            list1.add(scanner.nextInt());
        }
        System.out.print("Введите пять чисел для списка2: ");
        for (int i = 0; i < 5; i++) {
            list2.add(scanner.nextInt());
        }
        ArrayList summaryList = new ArrayList<>();
        summaryList = union(list1, list2);
        System.out.println(Arrays.toString(summaryList.toArray()));
    }

    public static ArrayList<Integer> union(ArrayList<Integer> list1, ArrayList<Integer> list2) {
        for (Integer l : list2) {
            list1.add(l);
        }
        return list1;
    }

}
