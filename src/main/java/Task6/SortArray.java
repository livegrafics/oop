package Task6;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;

public class SortArray {
    public static void main(String[] args) {
        int[] array=new int[100000];
        Random random=new Random(System.currentTimeMillis());
        for(int i=0;i<array.length;i++){
            array[i]=random.nextInt(array.length);
        }
        StopWatch stopWatch=new StopWatch();
        stopWatch.start();
        for (int i=0;i< array.length-1;i++){
            int max=array[i];
            int indexMax=i;
            for (int j=i+1;j<array.length;j++){
                if (max<array[j]){
                    max=array[j];
                    indexMax=j;
                }
            }
            if (indexMax!=i){
                int temp=array[i];
                array[i]=array[indexMax];
                array[indexMax]=temp;
            }
        }
        stopWatch.stop();
        System.out.println("Затрачено: " + stopWatch.getElapsedTime()+" ms");
    }
}
