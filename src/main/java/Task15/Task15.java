package Task15;

import java.math.BigInteger;

public class Task15 {
    public static void main(String[] args) {
        BigInteger bigInteger=new BigInteger(Long.toString(Long.MAX_VALUE));
        int count=0;
        while (count<10){
            bigInteger=bigInteger.add(new BigInteger("1"));
            if (bigInteger.mod(new BigInteger("5")).compareTo(new BigInteger("0"))==0 && count<10){
                System.out.println(bigInteger+" ������ 5");
                count++;
                continue;
            }
            if (bigInteger.mod(new BigInteger("6")).compareTo(new BigInteger("0"))==0 && count<10){
                System.out.println(bigInteger+" ������ 6");
                count++;
            }
        }
    }
}
