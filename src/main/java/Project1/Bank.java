package Project1;

import java.util.Scanner;

public class Bank {
    public static void main(String[] args) {
        Account account=new Account(1122,1000,5.5,"Герман");
        Scanner scanner=new Scanner(System.in);
        do{
           System.out.println("Введите ID:");
           int idAccount=scanner.nextInt();
           if (idAccount==1122){
               int menuItem;
               do{
                   outputMenu();
                   System.out.println("Введите пункт меню: ");
                   menuItem= scanner.nextInt();
                   double summ;
                   switch (menuItem){
                       case 1:
                           System.out.println("Баланс равен "+account.getBalance());
                           break;
                       case 2:
                           System.out.println("Введите сумму для снятия со счета: ");
                           summ=scanner.nextDouble();
                           account.withdraw(summ);
                           break;
                       case 3:
                           System.out.println("Введите сумму для внесения на счет: ");
                           summ=scanner.nextDouble();
                           account.deposit(summ);
                           break;
                       case 4:
                           printTransanctionList(account);
                           break;
                       case 5:
                           System.out.println("Спасибо за работу с банкоматом!\nДо свидания!");
                           break;
                       default:
                           System.out.println("Некорректный ввод!\nВыбран несуществующий пункт меню");
                   }
               }while(menuItem!=5);
           }
        }while (true);
    }
    public static void outputMenu(){
        System.out.println("Основное меню");
        System.out.println("1: проверить баланс счета");
        System.out.println("2: снять со счета");
        System.out.println("3: положить на счет");
        System.out.println("4: вывести сводку операций по счету");
        System.out.println("5: выйти");
    }

    public static void printTransanctionList(Account account){
        System.out.println();
        System.out.println("Сводка по счету");
        System.out.println("*************************");
        System.out.println("ID счета: "+account.getId());
        System.out.println("Владелец счета: "+account.getName());
        System.out.println("Процентная ставка: "+account.getAnnualInterestRate());
        System.out.println("Баланс: "+account.getBalance());
        System.out.println("*************************");
        System.out.println("Список операций по счету:");
        for (Transaction t:account.getTransactions()){
            System.out.println(t.toString());
        }
        System.out.println("*************************");
        System.out.println();
    }
}
