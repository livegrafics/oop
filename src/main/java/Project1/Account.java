package Project1;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;

public class Account {
    private int id;
    private double balance;
    private double annualInterestRate;
    private Date dateCreated;

    private ArrayList<Transaction> transactions = new ArrayList<>();

    private String name;

    public Account(int id, double balance) {
        this.id = id;
        this.balance = balance;
        dateCreated=new Date();
    }

    public Account(int id, double balance, double annualInterestRate, String name) {
        this.id = id;
        this.balance = balance;
        this.annualInterestRate = annualInterestRate;
        this.name = name;
    }

    public Account(int id, double balance, String name) {
        this.id = id;
        this.balance = balance;
        this.name = name;
    }

    public Account() {
        id=0;
        balance=0;
        annualInterestRate=0;
    }

    public double getMonthlyInterest(){
        double monthRate=annualInterestRate/12;
        return (int)(balance*monthRate/100*100)/100.0;
    }

    public void withdraw(double summ){
        balance=balance-summ;
        Transaction transaction =new Transaction('-',summ,balance,"Снятие денег со счета");
        this.transactions.add(transaction);

    }

    public void deposit(double summ){
        balance=balance+summ;
        Transaction transaction =new Transaction('+',summ,balance,"Пополнение счета");
        this.transactions.add(transaction);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getAnnualInterestRate() {
        return annualInterestRate;
    }

    public void setAnnualInterestRate(double annualInterestRate) {
        this.annualInterestRate = annualInterestRate;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    public String getName() {
        return name;
    }
}
