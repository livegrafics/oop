package Task1;

public class CreateRectangles {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(4,40);
        Rectangle rectangle2 = new Rectangle(3.5,35.9);
        System.out.println("Прямоугольник1: Ширина="+ rectangle1.width + " "+"Высота="
                +rectangle1.height + " " + "Площадь="+rectangle1.getArea()+" "+"Периметр="+rectangle1.getPerimeter());
        System.out.println("Прямоугольник2: Ширина="+ rectangle2.width + " "+"Высота="
                +rectangle2.height + " " + "Площадь="+rectangle2.getArea()+" "+"Периметр="+rectangle2.getPerimeter());
    }
}
