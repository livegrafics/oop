package Task24;

import java.util.ArrayList;
import java.util.Date;

public class Task24 {
    public static void main(String[] args) {
        ArrayList<Object> list = new ArrayList<>();
        list.add(new Loan());
        list.add(new Date());
        list.add(new Rectangle());
        for (Object o : list) {
            System.out.println(o.toString());
        }
    }
}
