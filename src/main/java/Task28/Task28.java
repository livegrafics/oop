package Task28;

import java.util.ArrayList;
import java.util.Scanner;

public class Task28 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Double> list = new ArrayList<>();
        System.out.print("Введите пять чисел: ");
        for (int i = 0; i < 5; i++) {
            list.add(scanner.nextDouble());
        }
        System.out.println("Сумма чисел: " + sum(list));
    }

    public static double sum(ArrayList<Double> list) {
        double sum = 0;
        for (double num : list) {
            sum = sum + num;
        }
        return sum;
    }
}
