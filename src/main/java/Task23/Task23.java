package Task23;

import java.util.ArrayList;
import java.util.Scanner;

public class Task23 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите последовательность чисел (если числа закончились - введите 0):");
        ArrayList<Integer> list = new ArrayList<>();
        Integer number;
        do {
            number = scanner.nextInt();
            if (number != 0) {
                list.add(number);
            }
        } while (number != 0);
        System.out.println("Максимальный элемент списка: " + max(list));
    }

    public static Integer max(ArrayList<Integer> list) {
        if (list == null || list.size() == 0) {
            return null;
        } else {
            Integer max = list.get(0);
            for (int i = 1; i < list.size(); i++) {
                if (list.get(i) > max) {
                    max = list.get(i);
                }
            }
            return max;
        }
    }

}
