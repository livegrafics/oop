package Task9;

public class MyInteger {
    private int value;

    public MyInteger(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public boolean isEven() {
        if (value % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isOdd() {
        if (value % 2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isPrime() {
        if (value > 1) {
            for (int i = 2; i < value;i++) {
                if (value % i == 0) {
                    return false;
                }
            }
            return true;
        }
        else return true;
    }

    public boolean equals(int value){
        if (value==this.value){
            return true;
        }else{
            return false;
        }
    }

    public boolean equals(MyInteger myInteger){
        if (myInteger.getValue()==this.value){
            return true;
        }else{
            return false;
        }
    }


    public static boolean isEven(int value) {
        if (value % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isOdd(int value) {
        if (value % 2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isPrime(int value) {
        if (value > 1) {
            for (int i = 2; i < value;i++) {
                if (value % i == 0) {
                    return false;
                }
            }
            return true;
        }
        else return true;
    }

    public static boolean isEven(MyInteger myInteger) {
        if (myInteger.getValue() % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isOdd(MyInteger myInteger) {
        if (myInteger.getValue() % 2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isPrime(MyInteger myInteger) {
        if (myInteger.getValue() > 1) {
            for (int i = 2; i < myInteger.getValue();i++) {
                if (myInteger.getValue() % i == 0) {
                    return false;
                }
            }
            return true;
        }
        else return true;
    }

    public static int parseInt(char[] charArray){
        return Integer.parseInt(String.valueOf(charArray));
    }

    public static int parseInt(String string){
        return Integer.parseInt(string);
    }
}
